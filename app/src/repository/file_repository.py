from flask import json


class FileRepository:

    def __init__(self):
        self.file = open("local_database.json", "a")

    def persist_object_to_file(self, object):
        json.dump(object.to_json(), self.file, ensure_ascii=False, indent=4)
        self.file.write(',')

    def close_file(self):
        self.file.close()
