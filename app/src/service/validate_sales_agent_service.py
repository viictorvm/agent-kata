
class ValidateSalesAgentService:
    """
    Method where we have all our logic to consider our data as validated.
    For example: duplicates should be removed
    """

    @staticmethod
    def execute(sales_agent_content_array):
        filtered_content_no_duplicates = list(set(sales_agent_content_array))

        return filtered_content_no_duplicates
