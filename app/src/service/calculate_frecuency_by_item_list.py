class CalculateFrecuencyByItemList:

    @staticmethod
    def exec(agent_object_list):
        dict_counter = {}
        for agent_object in agent_object_list:
            current_item = agent_object.item
            if current_item.id not in dict_counter:
                dict_counter[current_item.id] = 0
            dict_counter[current_item.id] += 1

        return dict_counter
