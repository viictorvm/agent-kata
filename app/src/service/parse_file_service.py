import csv
from io import StringIO

from customer_errors import FileNotValidError
from factory.create_sales_content_object_from_file import CreateSalesContentObjectFromFile


class ParseFileService:

    @staticmethod
    def execute(file):
        data_result = []
        if not file:
            raise FileNotValidError

        csvf = StringIO(file.read().decode())
        reader = csv.reader(csvf, delimiter='\n')
        for item in reader:
            content_object = CreateSalesContentObjectFromFile.create(item[0])
            data_result.append(content_object)

        return data_result
