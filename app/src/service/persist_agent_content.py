from repository.file_repository import FileRepository


class PersistAgentContentListService:

    @staticmethod
    def exec(agent_object_list):
        file_repository = FileRepository()
        for agent_object in agent_object_list:
            file_repository.persist_object_to_file(agent_object)
