class ParseSalesAgentContentArrayToJson:
    @staticmethod
    def transform(agent_content_array):
        json_content = []
        for agent_content in agent_content_array:
            json = agent_content.to_json()
            json_content.append(json)

        return {"result": json_content}
