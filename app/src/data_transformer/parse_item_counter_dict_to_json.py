class ParseItemCounterDictToJson:
    @staticmethod
    def transform(item_counter_dict):
        json_content = []
        for key in item_counter_dict:
            casted_key = key
            json_row = {"id": casted_key, "frequency": item_counter_dict[casted_key]}
            json_content.append(json_row)

        return {"items_frecuency": json_content}
