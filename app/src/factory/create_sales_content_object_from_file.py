from model.Item import Item
from model.sales_agent_content import SalesAgentContent
from customer_errors import InvalidArgument


class CreateSalesContentObjectFromFile:
    @staticmethod
    def create(row_from_file):
        if not row_from_file:
            raise InvalidArgument

        content = row_from_file.split(';')
        if len(content) < 7:
            raise InvalidArgument

        item = Item(sub_item_identification=content[0], item_words=content[1], name_item=content[2],
                    id_item=content[3], accuracy_item=content[4], validation_item=content[5]
                    )

        return SalesAgentContent(item=item, campaign_code=content[6], file=content[7])
