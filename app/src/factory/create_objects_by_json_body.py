from model.Item import Item
from customer_errors import InvalidArgument
from model.sales_agent_content import SalesAgentContent


class CreateItemObjectsByJsonBody:
    @staticmethod
    def create(json_body):
        agent_list = []
        if not json_body:
            raise InvalidArgument
        body = json_body.get("agent", None)

        if not body:
            raise InvalidArgument

        for key in body:
            current_item = key.get('item')
            id_item = current_item.get('id', None)
            sub_item_identification = current_item.get('subItemIdentification', None)
            item_words = current_item.get('words', None)
            name_item = current_item.get('name', None)
            accuracy_item = current_item.get('accuracy', None)
            validation_item = current_item.get('validation', None)
            item_object = Item(id_item=id_item, sub_item_identification=sub_item_identification, item_words=item_words,
                               name_item=name_item, accuracy_item=accuracy_item, validation_item=validation_item)

            campaign_code = key.get('campaignCode', None)
            file = key.get('file', None)
            agent_content = SalesAgentContent(item=item_object, campaign_code=campaign_code, file=file)

            agent_list.append(agent_content)

        return agent_list
