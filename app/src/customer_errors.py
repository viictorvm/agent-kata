from flask import Response


class FileNotValidError(Exception):
    pass


class InvalidArgument(Exception):
    pass


class RequestFailed(Response):
    def __init__(self):
        self.status_code = 400
        super().__init__()

