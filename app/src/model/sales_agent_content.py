from Model.Item import Item
from customer_errors import InvalidArgument


class SalesAgentContent:

    def __init__(self, item: Item, campaign_code, file):
        self.item = self.__check_is_none_or_raise_error(item)
        self.campaign_code = self.__check_is_none_or_raise_error(campaign_code)
        self.file = self.__check_is_none_or_raise_error(file)

    def __check_is_none_or_raise_error(self, value):
        if not value:
            raise InvalidArgument

        return value

    def __eq__(self, other):
        return self.item.__eq__(other.item) \
               and self.campaign_code == other.campaign_code \
               and self.file == other.file

    def __hash__(self):
        return True

    def to_json(self):
        return {
            'item': self.item.to_json(),
            'campaign_code': self.campaign_code,
            'file': self.file
        }
