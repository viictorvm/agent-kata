from customer_errors import InvalidArgument


class Item:
    def __init__(self, id_item, sub_item_identification, item_words, name_item, accuracy_item, validation_item):
        self.id = self.__check_is_none_or_raise_error(id_item)
        self.sub_item_identification = self.__check_is_none_or_raise_error(sub_item_identification)
        self.words = self.__check_is_none_or_raise_error(item_words)
        self.name = self.__check_is_none_or_raise_error(name_item)
        self.accuracy = self.__check_is_none_or_raise_error(accuracy_item)
        self.validation = self.__check_is_none_or_raise_error(validation_item)

    def __check_is_none_or_raise_error(self, value):
        if not value:
            raise InvalidArgument

        return value

    def to_json(self):
        return {
            'id': self.id,
            'sub_item_identification': self.sub_item_identification,
            'words': self.words,
            'name': self.name,
            'accuracy': self.accuracy,
            'validation': self.validation
        }

    def __hash__(self):
        return True

    def __eq__(self, other):
        return self.id == other.id \
               and self.sub_item_identification == other.sub_item_identification \
               and self.words == other.words and self.name == other.name \
               and self.accuracy == other.accuracy \
               and self.validation == other.validation
