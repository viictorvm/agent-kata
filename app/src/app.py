from flask import Flask, request, Response
from flask.json import dumps
from flask_api import status
from flask_cors import CORS

from handler.parse_file import ParseFile
from handler.frecuency_per_item_id import FrecuencyPerItemId
from handler.persist_agent_content import PersistAgentContent
from settings import FLASK_PORT, DEBUG, SECRET_KEY

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/file/parse/', methods=['POST'])
def parse_file():
    if not request.files:
        return 'No file found', status.HTTP_400_BAD_REQUEST

    parsed_content = ParseFile.handle(file=request.files)

    return Response(dumps(parsed_content), status=201, mimetype='application/json')


@app.route('/metric/frequency/', methods=['POST'])
def calculate_frecuency():
    json_body = request.json
    if not json_body:
        return 'error', status.HTTP_400_BAD_REQUEST
    calculate_frecuency = FrecuencyPerItemId.handle(json_body)

    return Response(dumps(calculate_frecuency), status=201, mimetype='application/json')


@app.route('/agentcontent/', methods=['POST'])
def persist():
    json_body = request.json
    if not json_body:
        return 'Content not valid', status.HTTP_400_BAD_REQUEST
    PersistAgentContent.handle(json_body)

    return Response(dumps('Inserted!'), status=201, mimetype='application/json')


if __name__ == '__main__':
    requestContext = app.test_request_context()
    app.secret_key = SECRET_KEY
    app.run(debug=DEBUG, host='0.0.0.0', port=FLASK_PORT)
