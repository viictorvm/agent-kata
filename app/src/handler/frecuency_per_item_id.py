from data_transformer.parse_item_counter_dict_to_json import ParseItemCounterDictToJson
from factory.create_objects_by_json_body import CreateItemObjectsByJsonBody
from service.calculate_frecuency_by_item_list import CalculateFrecuencyByItemList


class FrecuencyPerItemId:

    @staticmethod
    def handle(request_json_body):
        agent_list = CreateItemObjectsByJsonBody.create(request_json_body)
        frecuency_dict = CalculateFrecuencyByItemList.exec(agent_list)

        return ParseItemCounterDictToJson.transform(frecuency_dict)
