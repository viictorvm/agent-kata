from data_transformer.parse_sales_agent_content_to_json import ParseSalesAgentContentArrayToJson
from service.parse_file_service import ParseFileService
from service.validate_sales_agent_service import ValidateSalesAgentService


class ParseFile:
    @staticmethod
    def handle(file):
        content = ParseFileService.execute(file=file['file'])
        validated_content = ValidateSalesAgentService.execute(content)

        return ParseSalesAgentContentArrayToJson.transform(validated_content)
