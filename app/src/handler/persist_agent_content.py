from factory.create_objects_by_json_body import CreateItemObjectsByJsonBody
from service.persist_agent_content import PersistAgentContentListService


class PersistAgentContent:
    @staticmethod
    def handle(json_body):
        agent_list = CreateItemObjectsByJsonBody.create(json_body)
        PersistAgentContentListService.exec(agent_list)

        return json_body
