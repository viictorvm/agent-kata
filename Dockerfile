FROM python:3-alpine

ENV APPPATH /opt/myflaskapp
COPY . $APPPATH
WORKDIR $APPPATH
RUN pip install -r app/requirements.txt

