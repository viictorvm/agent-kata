# Agent-Kata - Flask
A simple Flask app running on Docker.

## Running docker compose.
1. Copy .env.dist to .env
2. It will build every container needed

```shell
cp .env.dist .env && make start
```

## Stack
1. Flask, as our main back app. (localhost:80/api)
2. Swagger, for a better API documentation. (go to localhost:80 to see the UI)
3. Nginx, we will need it if we want to deploy our app. 