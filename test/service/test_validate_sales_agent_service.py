import unittest

from Model.Item import Item
from Model.sales_agent_content import SalesAgentContent
from service.validate_sales_agent_service import ValidateSalesAgentService


class TestValidateSalesAgentService(unittest.TestCase):

    def test_validate_function_using_same_object_multiple_times_dif_references(self):
        words = {'hasta': 2, 'mañana': 1, 'febrero': 7, 'cargo': 3, 'desde': 3, 'gratuita': 1, 'gratuitos': 1,
                 'gratuito': 1}
        item = Item(id_item=1, sub_item_identification=2, item_words=words, name_item='Example', validation_item=2,
                    accuracy_item=3)
        item2 = Item(id_item=1, sub_item_identification=2, item_words=words, name_item='Example', validation_item=2,
                     accuracy_item=3)
        content = SalesAgentContent(item=item, campaign_code='4r4', file='example')
        content2 = SalesAgentContent(item=item2, campaign_code='4r4', file='example')

        example_array = [content, content2, content, content, content2]
        result = ValidateSalesAgentService.execute(example_array)

        self.assertEqual(len(example_array), 5)
        self.assertEqual(len(result), 1)

    def test_validate_function_using_same_object_multiple_times_same_references(self):
        words = {'hasta': 2, 'mañana': 1, 'febrero': 7, 'cargo': 3, 'desde': 3, 'gratuita': 1, 'gratuitos': 1,
                 'gratuito': 1}
        item = Item(id_item=1, sub_item_identification=2, item_words=words, name_item='Example', validation_item=2,
                    accuracy_item=3)
        content = SalesAgentContent(item=item, campaign_code='4r4', file='example')
        example_array = [content, content, content]
        result = ValidateSalesAgentService.execute(example_array)
        self.assertEqual(len(result), 1)
