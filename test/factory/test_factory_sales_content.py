import unittest

from Model.Item import Item
from customer_errors import InvalidArgument
from factory.create_sales_content_object_from_file import CreateSalesContentObjectFromFile


class TestFacotrySalesContentFromFile(unittest.TestCase):

    def test_create_object_with_empty_content_should_raise_invalid_argument_error(self):
        with self.assertRaises(InvalidArgument):
            CreateSalesContentObjectFromFile.create('')

    def test_create_object_with_less_arguments_should_raise_invalid_argument_error(self):
        with self.assertRaises(InvalidArgument):
            CreateSalesContentObjectFromFile.create(';')

    def test_create_object_with_argument_as_file_content(self):
        item = Item(sub_item_identification=1,
                    item_words={'hasta': 2, 'mañana': 1, 'febrero': 7, 'cargo': 3, 'desde': 3, 'gratuita': 1,
                                'gratuitos': 1, 'gratuito': 1}, name_item='cierreVenta', id_item=1,
                    accuracy_item=0.2763974028855432,
                    validation_item=1)

        object = CreateSalesContentObjectFromFile.create(
            "1;{'hasta':  2,  'mañana':  1,  'febrero':  7,  'cargo':  3,  'desde':  3,  'gratuita':  1,  'gratuitos':  1,  'gratuito':  1};cierreVenta;9;0.2763974028855432;1;0001-1260;100815299520_000231.wav")

        self.assertTrue(object.item.__eq__(item))
