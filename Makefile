UID=$(shell id -u)
GID=$(shell id -g)
DOCKER_SERVICE=flask

.PHONY: exec
exec:
	python3 app/src/app.py

.PHONY: start
start: stop erase build up -d

copy:
	pip freeze > requirements.txt

.PHONY: up
up: ## spin up environment
		UID=${UID} GID=${GID} docker-compose up

.PHONY: stop
stop:
		docker-compose stop

.PHONY: erase
erase:
		docker-compose down -v --remove-orphans

.PHONY: build
build:
		docker-compose build && \
		docker-compose pull

.PHONY: bash
bash:
		docker exec -it  ${DOCKER_SERVICE} sh

.PHONY: ps
ps:
		docker-compose ps
